<?php

use Illuminate\Support\Facades\Route;
use \App\Models\{
    User,
    Preference,
    Course,
    Module,
    Comment,
    Tag
};

Route::get('/', function () {
    return view('welcome');
});

Route::get('/one-to-one', function (){
    $user = User::with('preference')->find(2);

    $formData = [
        'background_color' => '#fff',
    ];

    if($user->preference){
      $user->preference->update($formData);
    }else{
        //$user->preference()->create($formData);
        $preference = new Preference($formData);
        $user->preference()->save($preference);
    }

    $user->refresh();

    var_dump($user->preference);

    $user->preference->delete();
    $user->refresh();

    dd($user->preference);
});
Route::get('/one-to-many', function (){
    //$course = Course::create(['title' => 'Curso de Laravel']);
    $course = Course::with('modules.lessons')->first();
    $module = Module::first();

    echo "<b>{$course->title}</b>";
    echo '<ul>';
    foreach ($course->modules as $module){
        echo "<li>Módulo: {$module->title}</li>";
        echo '<ul>';
        foreach ($module->lessons as $lesson) {
            echo "<li>Aula: {$lesson->title}</li>";
        }
        echo '</ul>';
    }
    echo '</ul>';

    /*$data = [
        'title' => 'Lesson 2',
        'video' => 'youtube.com/?watch=jklçjklçjkçl'
    ];*/

    //$module->lessons()->create($data);

    //$course->modules()->create($data);

    //$course->modules()->get();
    //$modules = $course->modules;

    //dd($modules);
});
Route::get('/many-to-many', function (){
    $user = User::with('permissions')->find(1);

    //$permission = Permission::first();
    //$user->permissions()->save($permission);
    /*$user->permissions()->saveMany([
        Permission::find(2),
        Permission::find(3)
    ]);*/
    //$user->permissions()->sync([1]);
    //$user->permissions()->attach([2,3]);
    $user->permissions()->detach([2,3]);

    $user->refresh();

    dd($user->permissions);
});
Route::get('/many-to-many-pivot', function (){
    $user = User::with('permissions')->find(1);
    $user->permissions()->attach([
        2
    ]);

    $user->refresh();

    echo "<b>{$user->name}</b><br>";
    echo "<ul>";
    foreach ($user->permissions as $permission) {
        echo "<li>{$permission->name} - {$permission->pivot->active}</li>";
    }
    echo "</ul>";
});
Route::get('/one-to-one-polymorphic', function (){
    $user = User::first();

    $data= ['path' => 'path/nome-image.png'];

    //$user->image->delete();

    $user->refresh();

    if($user->image){
        $user->image->update($data);
    }else {
        //$user->image()->save(new Image($data));
        $user->image()->create($data);
    }

    dd($user->image);
});
Route::get('/one-to-many-polymorphic', function (){
    $course = Course::with('comments')->first();

    /*$course->comments()->create([
        'subject' => 'Comentário 3',
        'content' => 'Conteúdo do comentário 3.'
    ]);

    $course->refresh();

    dd($course->comments);
    */

    $comment = Comment::find(1);
    dd($comment->commentable);
});
Route::get('/many-to-many-polymorphic', function (){
    /*$user = User::first();

    Tag::create(['name' => 'tag1', 'color' => 'blue']);
    Tag::create(['name' => 'tag2', 'color' => 'red']);
    Tag::create(['name' => 'tag3', 'color' => 'green']);

    $user->tags()->attach(2);
    $user->refresh();
    dd($user->tags);*/

    $tag = Tag::where('name', 'tag3')->first();
    dd($tag->users);
});
